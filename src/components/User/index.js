import {useContext} from 'react'
import {CharStats} from '../../context'
import './style.css'
const User = (props)=>{
    const {addStr,name,str, hp, speed} =useContext(CharStats)
    return(
        <div className='col-6'>
            <h1>{props.level}</h1>
            <p>Name:{name}</p>
            <p>str:{str}</p>
            <p>HP:{hp}</p>
            <p>speed:{speed}</p>
            <div onClick={()=>addStr(str+1)}>add +1 str</div>
        </div>
    )
}
export default User