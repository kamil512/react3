import { CharStats } from "../../context"
import { useContext } from 'react'
const ComponentPlayer = ()=>{
    const{name,str,hp} = useContext(CharStats)
    return(
        <div>
            <p>Name:{name}</p>
            <p>str:{str}</p>
            <p>HP:{hp}</p>
        </div>
    )
}
export default ComponentPlayer