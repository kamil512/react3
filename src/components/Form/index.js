import React, {useContext} from "react"
import {CharStats} from '../../context'
class Form extends React.Component{
    constructor(props){
        super(props)
        this.value = this.setState({value:''})
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.defaultValue1 = Math.round(Math.random()*20)
        this.defaultValue2 = Math.round(Math.random()*50)
        this.defaultValue3 = Math.round(Math.random()*70)
        this.showStats = false
    }
    handleChange(e){
        this.setState({value:e.target.value})
    }
    handleSubmit(e){
        this.showStats = true
        localStorage.setItem('name',this.state.value)
        localStorage.setItem('str',this.defaultValue1)
        localStorage.setItem('hp',this.defaultValue2)
        // changeOnTrue() // to nie zadziała
        e.preventDefault()
    }
    render(){
        return(
            
            <form onSubmit={this.handleSubmit}>
                <label htmlFor='name'>name:</label>
                <input type='text' name='name' value={this.value} onChange={this.handleChange} id='name'/>
                <label htmlFor='str'>str:</label>
                <input type='text' name='str' value={this.defaultValue1} readOnly={true} id='str'/>
                <label htmlFor='hp'>hp:</label>
                <input type='text' name='hp' value={this.defaultValue2} readOnly={true} id='hp'/>
                <label htmlFor='speed'>speed:</label>
                <input type='text' name='speed' value={this.defaultValue3} readOnly={true} id='speed'/>
                <input type='submit'/>
            </form>
        )
    }
}
export default Form
// const changeOnTrue = ()=>{
//    const [show,setShowStats] = useContext(CharStats)
//    setShowStats(!show)
// }