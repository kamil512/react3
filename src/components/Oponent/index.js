import { useContext } from 'react'
import {CharStats} from '../../context'
import './style.css'
import Form from '../Form'
import React from "react"
import ComponentPlayer from '../ComponentPlayer'

const Oponent = ()=>{
    let {showStats} =useContext(CharStats)
    return(
        <div className='col-6'>
            {showStats ?
            <ComponentPlayer/>:
            <Form/>}
        </div>
    )
}
export default Oponent