import React, {createContext,useState} from 'react';

export const CharStats = createContext({
    name:'Demon',
    str: 0,
    hp: 0,
    speed: 0,
    showStats: false,
    addStr: item=>{},
})

export const UserStatsProvider = ({ children }) =>{
    const name = 'Glolololo'
    const [str,addStr] = useState(10)
    const hp = 12
    const speed = 2
    return(
        <CharStats.Provider
            value={{
                name,
                str,
                hp,
                speed,
                addStr,
            }}
        >
            {children}
        </CharStats.Provider>
    )
}
export const OponentStatsProvider = ({ children }) =>{
    let name = 'Demon'
    let str = 0
    let hp = 0
    let showStats = false
    if(localStorage.getItem('name')){
        name = localStorage.getItem('name')
        str = localStorage.getItem('str')
        hp = localStorage.getItem('hp')
        showStats = true
        console.log(name,str,hp,showStats)
    }
    return(
        <CharStats.Provider
            value={{
                name,
                str,
                hp,
                showStats,
            }}
        >
            {children}
        </CharStats.Provider>
    )
}

//Provider służy do dostarczenia kontekstu swoim dzieciom
// Kontekst służy do wqspółdzileenia/przekazywania właściwości między komponentami