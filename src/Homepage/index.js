import User from '../components/User'
import Oponent from '../components/Oponent'
// import {useContext} from 'react'
// import Klasa from '../components/Klasa'
import {OponentStatsProvider, UserStatsProvider} from '../context'
const Homepage = ()=>{
    return(
        <div>
            <UserStatsProvider>
                <User level='10' />
            </UserStatsProvider>
            <OponentStatsProvider>
                <Oponent/>
            </OponentStatsProvider>
        </div>
    )
}
export default Homepage